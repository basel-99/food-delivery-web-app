from django.shortcuts import render
from rest_framework import viewsets, mixins, generics, serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Post, Likes
from .serializers import PostsSerializer, LikesSerializer
from .permissions import IsOwnerOrReadOnly
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import MultiPartParser, FileUploadParser
import datetime
from rest_framework.pagination import PageNumberPagination

class PostsViewset(viewsets.ModelViewSet):
    parser_classes = [MultiPartParser]
    permission_classes = [IsOwnerOrReadOnly]
    queryset = Post.objects.all().order_by('-id')
    serializer_class = PostsSerializer
    pagination_class = PageNumberPagination

class LieksViewset(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   mixins.CreateModelMixin,
                   mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = LikesSerializer
    queryset = Likes.objects.all()

class TopPosts(generics.ListAPIView):
    serializer_class = PostsSerializer
    # pagination_class = PageNumberPagination

    def get_queryset(self):
        today = datetime.datetime.today()
        rec = Post.objects.filter(date_posted__day=today.day).order_by('-like_cnt')
        return rec

class PostsForRestaurant(APIView):

    pagination_class = PageNumberPagination
    serializer_class = PostsSerializer

    @property
    def paginator(self):
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                self._paginator = self.pagination_class()
        else:
            pass
        return self._paginator

    def paginate_queryset(self, queryset):

        if self.paginator is None:
            return None
        return self.paginator.paginate_queryset(queryset,
                                                self.request, view=self)

    def get_paginated_response(self, data):
        assert self.paginator is not None
        return self.paginator.get_paginated_response(data)

    def get(self, request, format=None, *args, **kwargs):
        rest = request.data.get("restaurant", False)
        if not rest:
            raise serializers.ValidationError("No such id")
        instance = Post.objects.filter(author=rest).order_by('-id')
        page = self.paginate_queryset(instance)
        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page,many=True).data)
        else:
            serializer = self.serializer_class(instance, many=True)
        return Response(serializer.data)

class isLiked(APIView):

    def post(self, request, *args, **kwargs):
        post_id = request.data.get("Content").get("post", False)

        if post_id:
            user = request.user
            rec = Likes.objects.filter(post__id=post_id, person=user).first()
            if rec:
                return Response({1})
            else:
                return Response({0})
        return Response({0})

class Unlike(APIView):

    def post(self, request, *args, **kwargs):
        post_id = request.data.get("post", False)

        if post_id:
            Likes.objects.filter(post__id=post_id, person=request.user).first().delete()
            return Response("Done")
        else:
            return Response("Wrong Parameters")