import React from 'react';
import { Layout, Menu, Avatar, Dropdown, } from 'antd';
import {
    SettingFilled,
    LogoutOutlined,
    AppstoreOutlined,
    BarChartOutlined,
    CloudOutlined,
    ShopOutlined,
    TeamOutlined,
    UserOutlined,
    UploadOutlined,
    VideoCameraOutlined,
    DownOutlined,
  } from '@ant-design/icons';
  import { Link } from 'react-router-dom';

const { Header, Content, Footer, Sider } = Layout;


const LoginLayout = (props) => {
    return (
        <Layout>
          <Header >
      <Menu theme="dark" mode="horizontal">
        
      
        <Menu.Item>
          <Link to="/"> 
          <Avatar className="av"  size={58} src="https://comps.canstockphoto.com/frozen-food-premium-club-label-for-eps-vector_csp52386182.jpg" />
          </Link>
        </Menu.Item>
        
        <Menu.Item>
        <Link to="/"><text className="food">FORZEN</text></Link> 
        </Menu.Item>


        <Menu.Item className="h">
          <Link to="/">
            Home
          </Link>
        </Menu.Item>

        <Menu.Item className="l">
          <Link to="/resturant">
            Restaurants
            </Link>
        </Menu.Item>

        <Menu.Item className="c">
          <Link to="/contact">
            Contact Us
            </Link>
        </Menu.Item>
        
        <Menu.Item className="c">
        <Dropdown overlay={() => 
        <Menu >
          <Menu.Item>
         <Link to="/profile"> 
            <UserOutlined />&nbsp; User profile 
          </Link>
          </Menu.Item>
          
          <Menu.Item>
            <Link to="/login">
            <LogoutOutlined /> &nbsp;Logout 
            </Link>
          </Menu.Item>
        </Menu>}>
            <DownOutlined />

          
         </Dropdown>
        </Menu.Item>
    
      </Menu>
        


      <div class="input-group w-25 mx-auto ">
      <input type="text" className="form-control" placeholder="search your food..." />
        <div class="input-group-append">
        
         <button className="btn">Search</button>
      </div>
       
</div>
    </Header>



    <Sider
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
        <Menu.Item key="1">
          <UserOutlined />
          <span className="nav-text">nav 1</span>
        </Menu.Item>
        <Menu.Item key="2">
          <VideoCameraOutlined />
          <span className="nav-text">nav 2</span>
        </Menu.Item>
        <Menu.Item key="3">
          <UploadOutlined />
          <span className="nav-text">nav 3</span>
        </Menu.Item>
        <Menu.Item key="4">
          <BarChartOutlined />
          <span className="nav-text">nav 4</span>
        </Menu.Item>
        <Menu.Item key="5">
          <CloudOutlined />
          <span className="nav-text">nav 5</span>
        </Menu.Item>
        <Menu.Item key="6">
          <AppstoreOutlined />
          <span className="nav-text">nav 6</span>
        </Menu.Item>
        <Menu.Item key="7">
          <TeamOutlined />
          <span className="nav-text">nav 7</span>
        </Menu.Item>
        <Menu.Item key="8">
          <ShopOutlined />
          <span className="nav-text">nav 8</span>
        </Menu.Item>
      </Menu>
    </Sider>

    <Layout className="site-layout" style={{ marginLeft: 200 }}>

      <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
        <div className="site-layout-background" style={{ padding: 24, textAlign: 'center' }}>
          {props.children}
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
    </Layout>
  </Layout>
    );
}

export default LoginLayout;