import React, { useState,createElement } from 'react';
import {  Modal, Form, Input ,Menu,Tooltip} from 'antd';
import {  LikeOutlined,  LikeFilled } from '@ant-design/icons';
import Uploader from './Uploader';



const P = ({ visible, onCreate, onCancel }) => {
 
  const [form] = Form.useForm();
  const [likes, setLikes] = useState(0);
  
  const [action, setAction] = useState(null);
  const like = () => {
    setLikes(1);
    
    setAction('liked');
  };

 
  
  
 
  return (
    <Modal
     
      visible={visible}
      title="ADD A NEW POST"
      okText="Submit"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onCreate(values);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
      style={{ backgroundImage: "url(https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRfxan6VjGykYyYBSgfgJuiNTIIZMsSUal7gAPu5k61WTKVQ21l&usqp=CAU.png)",
      backgroundPosition:"center",
  backgroundSize: 'cover, 100%',
  backgroundRepeat: 'no-repeat',
   width:"100%",
   
      
      }}
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="title"
          label="Title:"
          rules={[
            {
              required: true,
              message: 'Please enter a Title!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item name="Content" label="The Content:" rules={[{required: true, message: 'Please enter The Content!',}, ]}>
          <Input type="textarea"/>
        </Form.Item>

        <Form.Item name="event" label="The Event:" rules={[{required: true, message: '',}, ]}>
        <span key="comment-basic-like">
      <Tooltip title="Like">
        {createElement(action === 'liked' ? LikeFilled : LikeOutlined, {
          onClick: like
        })}
      </Tooltip>
      <span className="comment-action">{likes}</span>
    </span>
    &nbsp; &nbsp;
    <span key="comment-basic-dislike">
     
      <span className="comment-action"></span>
    </span>
    &nbsp; &nbsp;
    <span key="comment-basic-reply-to">Reply to</span>
        </Form.Item>       

        <Form.Item name="img" label="The Image " rules={[{ required: true, message: 'Please enter the Image!!' },]}>
          <Uploader /> 
        </Form.Item>

      </Form>
    </Modal>
  );
};



const APost = () => {

  


  const [visible, setVisible] = useState(false);

  const onCreate = values => {
    console.log('Received values of form: ', values);
    setVisible(false);
  };
    
  return (
    <div>
     
      <Menu.Item
      
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
            style={{color:'#FFf'}}   >
        + Add Post
      </Menu.Item>
      <P
      
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};

export default APost;