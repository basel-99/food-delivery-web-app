import React from 'react';
import { Input } from 'antd';
import { render } from '@testing-library/react';

const { Search } = Input;


const SearchBar = (props) => {
render(

    <Search
      placeholder="input search text"
      enterButton="Search"
      size="large"
      onSearch={value => console.log(value)}
    />
    )
}

export default SearchBar;
