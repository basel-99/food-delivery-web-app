import React from 'react'


class SwitchDL extends React.Component{
    constructor(){
        super()
        this.state = {
            theme: 'dark',
            current: '1',
            name: '' , 
            language : 'EN' 
        }

        
    }

   /* changeTheme = value => {
        this.setState({
          theme: value ? 'dark' : 'light',
        });
      };
*/
    changeLanguage = value => {
        this.setState({
            language : value ? 'EN' : 'AR',
        });
    }  
    
      handleClick = e => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      };

      render() {
        return (
          <>
            <Switch
              checked={this.state.theme === 'dark'}
              onChange={this.changeTheme}
              checkedChildren="Dark"
              unCheckedChildren="Light"
            />
            </>
        );
      }
}
export default SwitchDL