import React, { useState } from 'react';
import {  Modal, Form, Input ,Menu} from 'antd';
import Uploader from './Uploader';



const Add = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  return (
    <Modal 
      visible={visible}
      title="ADD A NEW MEAL"
      okText="Add"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onCreate(values);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
 style={{ backgroundImage: "url(https://cdn0.iconfinder.com/data/icons/food-app-flat-style/128/Food_App_-_Flat_Style_-_33-42-512.png)",
      backgroundPosition:"center",
  backgroundSize: 'cover, 100%',
  backgroundRepeat: 'no-repeat',
   width:"100%",
   
      
      }}

        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="title"
          label="The name of the Meal :"
          rules={[
            {
              required: true,
              message: 'Please enter a meal name!!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item name="Price" label="The Price" rules={[{required: true, message: 'Please enter a meal Price!!',}, ]}>
          <Input type="textarea" placeholder="$" />
        </Form.Item>
        <Form.Item name="description" label="The Description" rules={[{ required: true, message: 'Please enter a meal description!!' },]}>
          <Input type="textarea" placeholder="شيش مشوي , بندورة , خيار , الخ" />
        </Form.Item>
        <Form.Item name="img" label="The Image " rules={[{ required: true, message: 'Please enter a meal Image!!' },]}>
          <Uploader />
        </Form.Item>

      </Form>
    </Modal>
  );
};

const AddMeal = () => {
  const [visible, setVisible] = useState(false);

  const onCreate = values => {
    console.log('Received values of form: ', values);
    setVisible(false);
  };

  return (
    <div>
     
      <Menu.Item
       
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
      >
        + ADD A MEAL
      </Menu.Item>
      <Add
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};

export default AddMeal;