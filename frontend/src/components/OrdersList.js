import React, { useState,} from 'react';
import {  Modal,  Menu} from 'antd';
//import { DislikeOutlined, LikeOutlined, DislikeFilled, LikeFilled } from '@ant-design/icons';
//import Uploader from './Uploader';
import OrdersListView from './OrdersListView'



const P = ({ visible, onCancel }) => {

 
  return (
    <Modal
      
      visible={visible}
      title="Our Orders"
      okText="OK"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={oncancel}
    >
      <OrdersListView />

    </Modal>
  );
};



const OrdersList = () => {
  const [visible, setVisible] = useState(false);

  const onCreate = values => {
    console.log('Received values of form: ', values);
    setVisible(false);
  };






  return (
    <div>

      <Menu.Item

        type="primary"
        onClick={() => {
          setVisible(true);
        }}
      >
        Our Orders
      </Menu.Item>
      <P
        visible={visible}
        onCreate={onCreate}
        
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};

export default OrdersList;