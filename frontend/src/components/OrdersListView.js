import { Menu } from 'antd';
import React from 'react'
//import {  AppstoreOutlined} from '@ant-design/icons';
import Modaljs from './Modaljs';

//import { Route , Link } from 'react-router-dom';
const { SubMenu } = Menu;


class OrdersListView extends React.Component {
  state = {
    name: ''
  };


  render() {
    return (
        <Menu
          theme="dark"
          style={{ width: 480 }}
          mode="inline"
        >
            <Menu.Item key="1"><Modaljs name = 'New Orders' /> </Menu.Item>
            <Menu.Item key="2"><Modaljs name = 'Orders in Process'/></Menu.Item>
            <Menu.Item key="3"><Modaljs name = 'Orders in Delivery' /></Menu.Item>
          
        </Menu>
    );
  }
}

export default OrdersListView